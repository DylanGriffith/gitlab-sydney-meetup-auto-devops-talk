slidenumbers: true

# GitLab

## Auto DevOps

---

# GitLab CI

---

# `.gitlab-ci.yml`

---

# Simple CI Job

```yml
job:
  script:
  - echo hello world!
```

---

# Run the tests

```yml
test:
  image: node
  script:
  - npm install
  - npm test
```

---

# Run the tests then deploy

```yml
stages:
  - test
  - deploy

test:
  stage: test
  image: node
  script:
  - npm install
  - npm test

deploy:
  stage: deploy
  image: kubectl
  script:
  - npm install
  - npm build
  - ./my-deployment-script
```

---

![fit](pipeline.png)

---

# DevOps maturity (what's next)

- Code Quality
- Staging deployments
- Review applications (for each branch)
- Security scanning
- Incremental rollouts

---

# Auto DevOps!

---

# Demo

---

# ¿Questions?

Quick Start guide: https://docs.gitlab.com/ee/topics/autodevops/quick_start_guide.html
Talk: https://gitlab.com/DylanGriffith/gitlab-sydney-meetup-auto-devops-talk
Project: https://gitlab.com/DylanGriffith/ado-gitlab-meetup-working

---
